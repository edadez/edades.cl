module.exports = {


  web: function (req, res) {
    return res.view('services/web');
  },

  mobile: function (req, res) {
    return res.view('services/mobile');
  },

  labs: function (req, res) {
    return res.view('services/labs');
  }


};

